# Clean your bloated device without root!

## Requirement

- ADB
>Depending on your package manager the package name might vary
```bash
yay -S android-sdk android-tools
```
- Android Mobile

# Commands 

## Device Management

Lists connected devices.
```bash
adb devices
```
Lists connected devices with detailed information.
```
adb devices -l
```
Reboots the device
```
adb reboot
```

## Shell Access

Opens a shell on your device where you can run various Linux commands.
```
adb shell
```
Lists installed apps and their package names.
```bash
adb shell pm list packages
```

## Logs

Shows device logs in real-time.
```
adb logcat
```
Clears the log buffer.
```
adb logcat -c 
```
Saves logs to a specified file.
```
adb logcat -f <filename>
```

## File Transfer

Pushes a file from your computer to the specified location on your device.
```
adb push <local> <remote>
```
Pulls a file from your device to your computer.
```
adb pull <remote> <local>
```

> For modify host file mobile must be rooted (Ads and unwanted websites)